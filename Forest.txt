# FamiTracker text export 0.4.2

# Song information
TITLE           "TinyGame"
AUTHOR          "Bright Moth"
COPYRIGHT       "2018"

# Song comment
COMMENT ""

# Global settings
MACHINE         0
FRAMERATE       0
EXPANSION       0
VIBRATO         1
SPLIT           32

# Macros
MACRO       0   0  -1  -1   0 : 10
MACRO       0   1  -1  -1   0 : 5 5 5 0 0 0 0 0
MACRO       0   2  -1  -1   0 : 5 5 5 5 5 5 5 5 0 0 0 0
MACRO       0   3  -1  -1   0 : 5 5 5 5 5 0 0 0
MACRO       0   4  -1  -1   0 : 0 0 0 5 5 5 5 5
MACRO       0   5  -1  -1   0 : 10 10 0 0 0 0 0 0
MACRO       0   6  -1  -1   0 : 4
MACRO       0   7  -1  -1   0 : 4 4 4 4 4 0 0 0
MACRO       0   8  -1  -1   0 : 6
MACRO       0   9  -1  -1   0 : 6 6 6 6 6 6 6 6 6 6 6 6 6 6 0
MACRO       0  10  -1  -1   0 : 6 6 6 6 6 6 6 6 6 6 6 6 6 6 6 6 0
MACRO       1   0   0   2   0 : 0 -9
MACRO       1   1   0   4   0 : 0 0 9 9
MACRO       1   2   0   4   0 : 0 0 8 8
MACRO       1   3  -1  -1   0 : 0 0 0 0 0 2 2 2 2 2 0 0 0 0 0 -2 -2 -2 -2 -2 -2 -2 -2
MACRO       1   4  -1  -1   0 : 0 0 2 2 4 4 5 5 7 7 9 9 10 10 12 12
MACRO       1   5  -1  -1   0 : 0 0 -2 -2 -4 -4 -5 -5 -7 -7 -9 -9 -10 -10 -12 -12
MACRO       2   0   0  -1   0 : 1 1 1 0 0 0 -1 -1 -1 0 0 0
MACRO       2   1   0  -1   0 : 1 1 1 0 0 0 -1 -1 -1 0 0 0
MACRO       4   0   0  -1   0 : 2 2
MACRO       4   1  -1  -1   0 : 2
MACRO       4   2  -1  -1   0 : 2

# DPCM samples

# Instruments
INST2A03   0     0  -1  -1  -1   0 "Beeps"
INST2A03   1     0  -1  -1  -1  -1 "S03"
INST2A03   2     5  -1  -1  -1  -1 "Noise"
INST2A03   3     1  -1  -1  -1  -1 "S03"
INST2A03   4     2  -1  -1  -1  -1 "S08"
INST2A03   5     3  -1  -1  -1  -1 "S05"
INST2A03   6     4  -1  -1  -1  -1 "G03"
INST2A03   7     6   1  -1  -1   0 "Arp 9"
INST2A03   8     6   2  -1  -1   0 "Arp 8"
INST2A03   9     6  -1  -1  -1   0 "Lead"
INST2A03  10     7  -1  -1  -1   0 "Lead S05"
INST2A03  11     8  -1   0  -1   1 "LeadSynth"
INST2A03  12     9  -1   0  -1   1 "LeadSynth S08"
INST2A03  13     8   3   0  -1   1 "LeadSynth DL "
INST2A03  14     8   4   1  -1   1 "LeadSynth RU"
INST2A03  15    10   5   1  -1   2 "LeadSynth RD"

# Tracks

TRACK  64   6 110 "forest"
COLUMNS : 1 1 1 1 1

ORDER 00 : 00 00 00 00 00
ORDER 01 : 00 00 01 00 00
ORDER 02 : 01 05 02 00 00
ORDER 03 : 01 05 03 00 00
ORDER 04 : 00 01 00 00 00
ORDER 05 : 00 02 01 00 00
ORDER 06 : 01 03 02 00 00
ORDER 07 : 01 04 03 00 00

PATTERN 00
ROW 00 : D-3 07 8 ... : G-2 09 4 ... : G-1 00 . ... : 1-# 02 8 ... : ... .. . ...
ROW 01 : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 02 : ... .. . ... : ... .. . ... : G-2 00 . ... : 1-# 02 . ... : ... .. . ...
ROW 03 : ... .. . ... : ... .. . ... : G-1 03 . ... : 1-# 02 . ... : ... .. . ...
ROW 04 : ... .. . ... : ... .. . ... : G-1 00 . ... : C-# 02 . ... : ... .. . ...
ROW 05 : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 06 : ... .. . ... : ... .. . ... : D-1 03 . ... : 1-# 02 . ... : ... .. . ...
ROW 07 : ... .. . ... : ... .. . ... : G-1 05 . ... : 1-# 02 . ... : ... .. . ...
ROW 08 : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 09 : ... .. . ... : ... .. . ... : D-1 00 . ... : 1-# 02 . ... : ... .. . ...
ROW 0A : ... .. . ... : ... .. . ... : G-1 00 . ... : 1-# 02 . ... : ... .. . ...
ROW 0B : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 0C : ... .. . ... : ... .. . ... : ... .. . ... : C-# 02 . ... : ... .. . ...
ROW 0D : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 0E : ... .. . ... : ... .. . ... : D-2 00 . ... : 1-# 02 . ... : ... .. . ...
ROW 0F : ... .. . ... : G-2 0A . ... : E-2 00 . ... : ... .. . ... : ... .. . ...
ROW 10 : E-3 08 . ... : G-2 09 . ... : G-1 00 . ... : 1-# 02 . ... : ... .. . ...
ROW 11 : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 12 : ... .. . ... : ... .. . ... : D-1 03 . ... : 1-# 02 . ... : ... .. . ...
ROW 13 : ... .. . ... : ... .. . ... : G-1 03 . ... : 1-# 02 . ... : ... .. . ...
ROW 14 : ... .. . ... : ... .. . ... : G-1 00 . ... : C-# 02 . ... : ... .. . ...
ROW 15 : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 16 : ... .. . ... : ... .. . ... : D-1 03 . ... : 1-# 02 . ... : ... .. . ...
ROW 17 : ... .. . ... : ... .. . ... : G-1 04 . ... : 1-# 02 . ... : ... .. . ...
ROW 18 : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 19 : ... .. . ... : ... .. . ... : G-1 00 . ... : C-# 02 . ... : ... .. . ...
ROW 1A : ... .. . ... : ... .. . ... : D-1 03 . ... : 1-# 02 . ... : ... .. . ...
ROW 1B : ... .. . ... : ... .. . ... : D-1 03 . ... : 1-# 02 . ... : ... .. . ...
ROW 1C : ... .. . ... : ... .. . ... : B-1 03 . ... : C-# 02 . ... : ... .. . ...
ROW 1D : ... .. . ... : ... .. . ... : B-1 00 . ... : ... .. . ... : ... .. . ...
ROW 1E : ... .. . ... : ... .. . ... : D-1 03 . ... : 1-# 02 . ... : ... .. . ...
ROW 1F : ... .. . ... : G-2 0A . ... : D-1 03 . ... : ... .. . ... : ... .. . ...
ROW 20 : F-3 07 . ... : G-2 09 . ... : G-1 00 . ... : 1-# 02 . ... : ... .. . ...
ROW 21 : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 22 : ... .. . ... : ... .. . ... : G-2 00 . ... : 1-# 02 . ... : ... .. . ...
ROW 23 : ... .. . ... : ... .. . ... : G-1 03 . ... : 1-# 02 . ... : ... .. . ...
ROW 24 : ... .. . ... : ... .. . ... : G-1 00 . ... : C-# 02 . ... : ... .. . ...
ROW 25 : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 26 : ... .. . ... : ... .. . ... : D-1 03 . ... : 1-# 02 . ... : ... .. . ...
ROW 27 : ... .. . ... : ... .. . ... : G-1 05 . ... : 1-# 02 . ... : ... .. . ...
ROW 28 : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 29 : ... .. . ... : ... .. . ... : D-1 00 . ... : 1-# 02 . ... : ... .. . ...
ROW 2A : ... .. . ... : ... .. . ... : G-1 00 . ... : 1-# 02 . ... : ... .. . ...
ROW 2B : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 2C : ... .. . ... : ... .. . ... : ... .. . ... : C-# 02 . ... : ... .. . ...
ROW 2D : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 2E : ... .. . ... : ... .. . ... : D-2 00 . ... : 1-# 02 . ... : ... .. . ...
ROW 2F : ... .. . ... : G-2 0A . ... : E-2 00 . ... : ... .. . ... : ... .. . ...
ROW 30 : E-3 08 . ... : G-2 09 . ... : G-1 00 . ... : 1-# 02 . ... : ... .. . ...
ROW 31 : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 32 : ... .. . ... : ... .. . ... : D-1 03 . ... : 1-# 02 . ... : ... .. . ...
ROW 33 : ... .. . ... : ... .. . ... : G-1 03 . ... : 1-# 02 . ... : ... .. . ...
ROW 34 : ... .. . ... : ... .. . ... : G-1 00 . ... : C-# 02 . ... : ... .. . ...
ROW 35 : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 36 : ... .. . ... : ... .. . ... : D-1 06 . ... : 1-# 02 . ... : ... .. . ...
ROW 37 : ... .. . ... : ... .. . ... : G-1 04 . ... : 1-# 02 . ... : ... .. . ...
ROW 38 : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 39 : ... .. . ... : ... .. . ... : G-1 03 . ... : C-# 02 . ... : ... .. . ...
ROW 3A : ... .. . ... : ... .. . ... : E-1 04 . ... : 1-# 02 . ... : ... .. . ...
ROW 3B : ... .. . ... : ... .. . ... : ... .. . ... : 1-# 02 . ... : ... .. . ...
ROW 3C : ... .. . ... : ... .. . ... : E-1 04 . ... : C-# 02 . ... : ... .. . ...
ROW 3D : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 3E : ... .. . ... : ... .. . ... : F-1 04 . ... : 1-# 02 . ... : ... .. . ...
ROW 3F : ... .. . ... : G-2 0A . ... : ... .. . ... : ... .. . ... : ... .. . ...

PATTERN 01
ROW 00 : G-3 07 . ... : B-3 0B 8 ... : G-1 00 . ... : ... .. . ... : ... .. . ...
ROW 01 : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 02 : ... .. . ... : ... .. . ... : G-2 00 . ... : ... .. . ... : ... .. . ...
ROW 03 : ... .. . ... : ... .. . ... : G-1 03 . ... : ... .. . ... : ... .. . ...
ROW 04 : ... .. . ... : ... .. . ... : G-1 00 . ... : ... .. . ... : ... .. . ...
ROW 05 : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 06 : ... .. . ... : ... .. . ... : D-1 03 . ... : ... .. . ... : ... .. . ...
ROW 07 : ... .. . ... : ... .. . ... : G-1 05 . ... : ... .. . ... : ... .. . ...
ROW 08 : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 09 : ... .. . ... : ... .. . ... : D-1 00 . ... : ... .. . ... : ... .. . ...
ROW 0A : ... .. . ... : ... .. . ... : G-1 00 . ... : ... .. . ... : ... .. . ...
ROW 0B : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 0C : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 0D : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 0E : ... .. . ... : C-4 0B . ... : D-2 00 . ... : ... .. . ... : ... .. . ...
ROW 0F : ... .. . ... : ... .. . ... : E-2 00 . ... : ... .. . ... : ... .. . ...
ROW 10 : A-3 08 . ... : B-3 0B . ... : G-1 00 . ... : ... .. . ... : ... .. . ...
ROW 11 : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 12 : ... .. . ... : ... .. . ... : D-1 03 . ... : ... .. . ... : ... .. . ...
ROW 13 : ... .. . ... : ... .. . ... : G-1 03 . ... : ... .. . ... : ... .. . ...
ROW 14 : ... .. . ... : ... .. . ... : G-1 00 . ... : ... .. . ... : ... .. . ...
ROW 15 : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 16 : ... .. . ... : ... .. . ... : D-1 06 . ... : ... .. . ... : ... .. . ...
ROW 17 : ... .. . ... : ... .. . ... : G-1 04 . ... : ... .. . ... : ... .. . ...
ROW 18 : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 19 : ... .. . ... : ... .. . ... : G-1 00 . ... : ... .. . ... : ... .. . ...
ROW 1A : ... .. . ... : ... .. . ... : E-1 02 . ... : ... .. . ... : ... .. . ...
ROW 1B : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 1C : ... .. . ... : C-4 0B . ... : G-1 00 . ... : ... .. . ... : ... .. . ...
ROW 1D : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 1E : ... .. . ... : B-3 0B . ... : E-1 00 . ... : ... .. . ... : ... .. . ...
ROW 1F : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 20 : B-3 08 . ... : D-4 0B . ... : G-1 00 . ... : ... .. . ... : ... .. . ...
ROW 21 : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 22 : ... .. . ... : ... .. . ... : G-2 00 . ... : ... .. . ... : ... .. . ...
ROW 23 : ... .. . ... : ... .. . ... : G-1 03 . ... : ... .. . ... : ... .. . ...
ROW 24 : ... .. . ... : ... .. . ... : G-1 00 . ... : ... .. . ... : ... .. . ...
ROW 25 : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 26 : ... .. . ... : ... .. . ... : D-1 03 . ... : ... .. . ... : ... .. . ...
ROW 27 : ... .. . ... : ... .. . ... : G-1 05 . ... : ... .. . ... : ... .. . ...
ROW 28 : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 29 : ... .. . ... : ... .. . ... : D-1 00 . ... : ... .. . ... : ... .. . ...
ROW 2A : ... .. . ... : ... .. . ... : G-1 00 . ... : ... .. . ... : ... .. . ...
ROW 2B : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 2C : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 2D : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 2E : ... .. . ... : E-4 0B . ... : D-2 00 . ... : ... .. . ... : ... .. . ...
ROW 2F : ... .. . ... : ... .. . ... : E-2 00 . ... : ... .. . ... : ... .. . ...
ROW 30 : A-3 08 . ... : D-4 0B . ... : G-1 00 . ... : ... .. . ... : ... .. . ...
ROW 31 : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 32 : ... .. . ... : ... .. . ... : G-2 00 . ... : ... .. . ... : ... .. . ...
ROW 33 : ... .. . ... : ... .. . ... : D-1 00 . ... : ... .. . ... : ... .. . ...
ROW 34 : ... .. . ... : ... .. . ... : G-1 03 . ... : ... .. . ... : ... .. . ...
ROW 35 : ... .. . ... : ... .. . ... : E-2 00 . ... : ... .. . ... : ... .. . ...
ROW 36 : ... .. . ... : ... .. . ... : D-1 00 . ... : ... .. . ... : ... .. . ...
ROW 37 : ... .. . ... : ... .. . ... : G-1 03 . ... : ... .. . ... : ... .. . ...
ROW 38 : ... .. . ... : ... .. . ... : G-1 00 . ... : ... .. . ... : ... .. . ...
ROW 39 : ... .. . ... : ... .. . ... : D-1 00 . ... : ... .. . ... : ... .. . ...
ROW 3A : ... .. . ... : ... .. . ... : B-1 00 . ... : ... .. . ... : ... .. . ...
ROW 3B : ... .. . ... : ... .. . ... : G-1 03 . ... : ... .. . ... : ... .. . ...
ROW 3C : ... .. . ... : ... .. . ... : G-1 00 . ... : ... .. . ... : ... .. . ...
ROW 3D : ... .. . ... : ... .. . ... : A-1 00 . ... : ... .. . ... : ... .. . ...
ROW 3E : ... .. . ... : ... .. . ... : B-1 00 . ... : ... .. . ... : ... .. . ...
ROW 3F : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...

PATTERN 02
ROW 00 : ... .. . ... : ... .. 0 ... : C-2 00 . ... : ... .. . ... : ... .. . ...
ROW 01 : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 02 : ... .. . ... : D-4 0C 8 ... : C-3 00 . ... : ... .. . ... : ... .. . ...
ROW 03 : ... .. . ... : ... .. . ... : C-2 03 . ... : ... .. . ... : ... .. . ...
ROW 04 : ... .. . ... : ... .. . ... : C-2 00 . ... : ... .. . ... : ... .. . ...
ROW 05 : ... .. . ... : D-4 0B . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 06 : ... .. . ... : ... .. . ... : G-1 03 . ... : ... .. . ... : ... .. . ...
ROW 07 : ... .. . ... : ... .. . ... : C-2 05 . ... : ... .. . ... : ... .. . ...
ROW 08 : ... .. . ... : C-4 0B . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 09 : ... .. . ... : ... .. . ... : G-1 00 . ... : ... .. . ... : ... .. . ...
ROW 0A : ... .. . ... : ... .. . ... : C-2 00 . ... : ... .. . ... : ... .. . ...
ROW 0B : ... .. . ... : D-4 0B . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 0C : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 0D : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 0E : ... .. . ... : C-4 0B . ... : G-2 00 . ... : ... .. . ... : ... .. . ...
ROW 0F : ... .. . ... : ... .. . ... : A-2 00 . ... : ... .. . ... : ... .. . ...
ROW 10 : ... .. . ... : B-3 0B . ... : C-2 00 . ... : ... .. . ... : ... .. . ...
ROW 11 : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 12 : ... .. . ... : ... .. . ... : G-1 03 . ... : ... .. . ... : ... .. . ...
ROW 13 : ... .. . ... : ... .. . ... : C-2 03 . ... : ... .. . ... : ... .. . ...
ROW 14 : ... .. . ... : ... .. . ... : C-2 00 . ... : ... .. . ... : ... .. . ...
ROW 15 : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 16 : ... .. . ... : ... .. . ... : G-1 03 . ... : ... .. . ... : ... .. . ...
ROW 17 : ... .. . ... : ... .. . ... : C-2 04 . ... : ... .. . ... : ... .. . ...
ROW 18 : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 19 : ... .. . ... : ... .. . ... : C-2 00 . ... : ... .. . ... : ... .. . ...
ROW 1A : ... .. . ... : ... .. . ... : G-1 03 . ... : ... .. . ... : ... .. . ...
ROW 1B : ... .. . ... : ... .. . ... : G-1 03 . ... : ... .. . ... : ... .. . ...
ROW 1C : ... .. . ... : ... .. . ... : E-2 03 . ... : ... .. . ... : ... .. . ...
ROW 1D : ... .. . ... : ... .. . ... : E-2 00 . ... : ... .. . ... : ... .. . ...
ROW 1E : ... .. . ... : A-3 0D . ... : G-1 03 . ... : ... .. . ... : ... .. . ...
ROW 1F : ... .. . ... : ... .. . ... : G-1 03 . ... : ... .. . ... : ... .. . ...
ROW 20 : ... .. . ... : ... .. . ... : C-2 00 . ... : ... .. . ... : ... .. . ...
ROW 21 : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 22 : ... .. . ... : ... .. . ... : C-3 00 . ... : ... .. . ... : ... .. . ...
ROW 23 : ... .. . ... : ... .. . ... : C-2 03 . ... : ... .. . ... : ... .. . ...
ROW 24 : ... .. . ... : ... .. . ... : C-2 00 . ... : ... .. . ... : ... .. . ...
ROW 25 : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 26 : ... .. . ... : ... .. . ... : G-1 03 . ... : ... .. . ... : ... .. . ...
ROW 27 : ... .. . ... : ... .. . ... : C-2 05 . ... : ... .. . ... : ... .. . ...
ROW 28 : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 29 : ... .. . ... : ... .. . ... : G-1 00 . ... : ... .. . ... : ... .. . ...
ROW 2A : ... .. . ... : ... .. . ... : C-2 00 . ... : ... .. . ... : ... .. . ...
ROW 2B : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 2C : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 2D : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 2E : ... .. . ... : ... .. . ... : G-2 00 . ... : ... .. . ... : ... .. . ...
ROW 2F : ... .. . ... : ... .. . ... : A-2 00 . ... : ... .. . ... : ... .. . ...
ROW 30 : ... .. . ... : ... .. . ... : C-2 00 . ... : ... .. . ... : ... .. . ...
ROW 31 : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 32 : ... .. . ... : ... .. . ... : G-1 03 . ... : ... .. . ... : ... .. . ...
ROW 33 : ... .. . ... : ... .. . ... : C-2 03 . ... : ... .. . ... : ... .. . ...
ROW 34 : ... .. . ... : ... .. . ... : C-2 00 . ... : ... .. . ... : ... .. . ...
ROW 35 : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 36 : ... .. . ... : ... .. . ... : G-1 06 . ... : ... .. . ... : ... .. . ...
ROW 37 : ... .. . ... : ... .. . ... : C-2 04 . ... : ... .. . ... : ... .. . ...
ROW 38 : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 39 : ... .. . ... : ... .. . ... : C-2 03 . ... : ... .. . ... : ... .. . ...
ROW 3A : ... .. . ... : ... .. . ... : A-1 04 . ... : ... .. . ... : ... .. . ...
ROW 3B : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 3C : ... .. . ... : ... .. . ... : A-1 04 . ... : ... .. . ... : ... .. . ...
ROW 3D : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 3E : ... .. . ... : G-3 0E . ... : B-1 04 . ... : ... .. . ... : ... .. . ...
ROW 3F : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...

PATTERN 03
ROW 00 : ... .. . ... : G-4 0B . ... : C-2 00 . ... : ... .. . ... : ... .. . ...
ROW 01 : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 02 : ... .. . ... : ... .. . ... : C-3 00 . ... : ... .. . ... : ... .. . ...
ROW 03 : ... .. . ... : ... .. . ... : C-2 03 . ... : ... .. . ... : ... .. . ...
ROW 04 : ... .. . ... : ... .. . ... : C-2 00 . ... : ... .. . ... : ... .. . ...
ROW 05 : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 06 : ... .. . ... : ... .. . ... : G-1 03 . ... : ... .. . ... : ... .. . ...
ROW 07 : ... .. . ... : ... .. . ... : C-2 05 . ... : ... .. . ... : ... .. . ...
ROW 08 : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 09 : ... .. . ... : ... .. . ... : G-1 00 . ... : ... .. . ... : ... .. . ...
ROW 0A : ... .. . ... : ... .. . ... : C-2 00 . ... : ... .. . ... : ... .. . ...
ROW 0B : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 0C : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 0D : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 0E : ... .. . ... : ... .. . ... : G-2 00 . ... : ... .. . ... : ... .. . ...
ROW 0F : ... .. . ... : ... .. . ... : A-2 00 . ... : ... .. . ... : ... .. . ...
ROW 10 : ... .. . ... : ... .. . ... : C-2 00 . ... : ... .. . ... : ... .. . ...
ROW 11 : ... .. . ... : ... .. 0 ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 12 : ... .. . ... : G-4 0B 8 ... : G-1 03 . ... : ... .. . ... : ... .. . ...
ROW 13 : ... .. . ... : ... .. . ... : C-2 03 . ... : ... .. . ... : ... .. . ...
ROW 14 : ... .. . ... : ... .. . ... : C-2 00 . ... : ... .. . ... : ... .. . ...
ROW 15 : ... .. . ... : F-4 0B . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 16 : ... .. . ... : ... .. . ... : G-1 06 . ... : ... .. . ... : ... .. . ...
ROW 17 : ... .. . ... : ... .. . ... : C-2 04 . ... : ... .. . ... : ... .. . ...
ROW 18 : ... .. . ... : E-4 0B . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 19 : ... .. . ... : ... .. . ... : C-2 00 . ... : ... .. . ... : ... .. . ...
ROW 1A : ... .. . ... : ... .. . ... : A-1 02 . ... : ... .. . ... : ... .. . ...
ROW 1B : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 1C : ... .. . ... : ... .. . ... : C-2 00 . ... : ... .. . ... : ... .. . ...
ROW 1D : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 1E : ... .. . ... : F-4 0B . ... : A-1 00 . ... : ... .. . ... : ... .. . ...
ROW 1F : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 20 : ... .. . ... : E-4 0B . ... : C-2 00 . ... : ... .. . ... : ... .. . ...
ROW 21 : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 22 : ... .. . ... : ... .. . ... : C-3 00 . ... : ... .. . ... : ... .. . ...
ROW 23 : ... .. . ... : ... .. . ... : C-2 03 . ... : ... .. . ... : ... .. . ...
ROW 24 : ... .. . ... : ... .. . ... : C-2 00 . ... : ... .. . ... : ... .. . ...
ROW 25 : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 26 : ... .. . ... : ... .. . ... : G-1 03 . ... : ... .. . ... : ... .. . ...
ROW 27 : ... .. . ... : ... .. . ... : C-2 05 . ... : ... .. . ... : ... .. . ...
ROW 28 : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 29 : ... .. . ... : ... .. . ... : G-1 00 . ... : ... .. . ... : ... .. . ...
ROW 2A : ... .. . ... : ... .. . ... : C-2 00 . ... : ... .. . ... : ... .. . ...
ROW 2B : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 2C : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 2D : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 2E : ... .. . ... : F-4 0B . ... : G-2 00 . ... : ... .. . ... : ... .. . ...
ROW 2F : ... .. . ... : ... .. . ... : A-2 00 . ... : ... .. . ... : ... .. . ...
ROW 30 : ... .. . ... : G-4 0B . ... : C-2 00 . ... : ... .. . ... : ... .. . ...
ROW 31 : ... .. . ... : ... .. . ... : G-2 05 . ... : ... .. . ... : ... .. . ...
ROW 32 : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 33 : ... .. . ... : ... .. . ... : G-2 04 . ... : ... .. . ... : ... .. . ...
ROW 34 : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 35 : ... .. . ... : ... .. . ... : F-2 04 . ... : ... .. . ... : ... .. . ...
ROW 36 : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 37 : ... .. . ... : ... .. . ... : E-2 04 . ... : ... .. . ... : ... .. . ...
ROW 38 : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 39 : ... .. . ... : ... .. . ... : E-2 04 . ... : ... .. . ... : ... .. . ...
ROW 3A : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 3B : ... .. . ... : ... .. . ... : D-2 04 . ... : ... .. . ... : ... .. . ...
ROW 3C : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 3D : ... .. . ... : ... .. . ... : D-2 04 . ... : ... .. . ... : ... .. . ...
ROW 3E : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 3F : ... .. . ... : ... .. . ... : C-2 04 . ... : ... .. . ... : ... .. . ...

PATTERN 04
ROW 00 : ... .. . ... : ... .. 0 ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 01 : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 02 : ... .. . ... : G-4 0C 8 ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 03 : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 04 : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 05 : ... .. . ... : G-4 0B . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 06 : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 07 : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 08 : ... .. . ... : F-4 0B . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 09 : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 0A : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 0B : ... .. . ... : G-4 0B . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 0C : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 0D : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 0E : ... .. . ... : F-4 0B . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 0F : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 10 : ... .. . ... : E-4 0B . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 11 : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 12 : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 13 : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 14 : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 15 : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 16 : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 17 : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 18 : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 19 : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 1A : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 1B : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 1C : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 1D : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 1E : ... .. . ... : D-4 0D . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 1F : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 20 : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 21 : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 22 : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 23 : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 24 : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 25 : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 26 : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 27 : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 28 : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 29 : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 2A : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 2B : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 2C : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 2D : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 2E : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 2F : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 30 : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 31 : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 32 : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 33 : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 34 : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 35 : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 36 : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 37 : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 38 : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 39 : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 3A : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 3B : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 3C : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 3D : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 3E : ... .. . ... : C-4 0F . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 3F : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...

PATTERN 05
ROW 00 : ... .. . ... : C-3 09 4 ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 01 : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 02 : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 03 : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 04 : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 05 : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 06 : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 07 : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 08 : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 09 : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 0A : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 0B : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 0C : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 0D : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 0E : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 0F : ... .. . ... : C-3 0A . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 10 : ... .. . ... : C-3 09 . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 11 : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 12 : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 13 : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 14 : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 15 : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 16 : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 17 : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 18 : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 19 : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 1A : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 1B : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 1C : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 1D : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 1E : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 1F : ... .. . ... : C-3 0A . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 20 : ... .. . ... : C-3 09 . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 21 : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 22 : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 23 : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 24 : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 25 : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 26 : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 27 : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 28 : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 29 : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 2A : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 2B : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 2C : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 2D : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 2E : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 2F : ... .. . ... : C-3 0A . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 30 : ... .. . ... : C-3 09 . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 31 : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 32 : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 33 : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 34 : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 35 : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 36 : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 37 : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 38 : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 39 : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 3A : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 3B : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 3C : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 3D : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 3E : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 3F : ... .. . ... : C-3 0A . ... : ... .. . ... : ... .. . ... : ... .. . ...

TRACK  64   6 150 "sfx_fire"
COLUMNS : 1 1 1 1 1

ORDER 00 : 00 00 00 00 00

PATTERN 00
ROW 00 : ... .. . ... : ... .. . ... : ... .. . ... : 1-# 00 7 ... : ... .. . ...
ROW 01 : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 02 : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 03 : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 04 : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . C00 : ... .. . ...
ROW 05 : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 06 : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 07 : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 08 : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 09 : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 0A : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 0B : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 0C : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 0D : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 0E : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 0F : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 10 : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 11 : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 12 : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 13 : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 14 : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 15 : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 16 : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 17 : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 18 : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 19 : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 1A : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 1B : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 1C : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 1D : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 1E : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 1F : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 20 : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 21 : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 22 : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 23 : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 24 : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 25 : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 26 : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 27 : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 28 : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 29 : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 2A : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 2B : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 2C : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 2D : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 2E : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 2F : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 30 : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 31 : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 32 : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 33 : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 34 : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 35 : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 36 : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 37 : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 38 : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 39 : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 3A : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 3B : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 3C : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 3D : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 3E : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...
ROW 3F : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ... : ... .. . ...

# End of export
