; prng
;
; Returns a random 8-bit number in A (0-255), clobbers X (0).
;
; Requires a 2-byte value on the zero page called "rngSeed".
; Initialize seed to any value except 0 before the first call to prng.
; (A seed value of 0 will cause prng to always return 0.)
;
; This is a 16-bit Galois linear feedback shift register with polynomial $002D.
; The sequence of numbers it generates will repeat after 65535 calls.
;
; Execution time is an average of 125 cycles (excluding jsr and rts)

;.zeropage
;rngSeed: .res 2       ; initialize 16-bit seed to any value except 0

prng:
	LDX #8     ; iteration count (generates 8 bits)
	LDA rngSeed+0
.a:
	ASL A       ; shift the register
	ROL rngSeed+1
	BCC .a
	EOR #$2D   ; apply XOR feedback whenever a 1 bit is shifted out
.b:
	DEX
	BNE .b
	STA rngSeed+0
	CMP #0     ; reload flags
	RTS
	