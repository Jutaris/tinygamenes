song_index_forest = 0

sfx_index_sfx_fire = 0

song_list:
  .dw _forest

sfx_list:
  .dw _sfx_fire

instrument_list:
  .dw Beeps_0
  .dw S03_1
  .dw Noise_2
  .dw S03_3
  .dw S08_4
  .dw S05_5
  .dw G03_6
  .dw Arp_9_7
  .dw Arp_8_8
  .dw Lead_9
  .dw Lead_S05_10
  .dw LeadSynth_11
  .dw LeadSynth_S08_12
  .dw LeadSynth_DL__13
  .dw LeadSynth_RU_14
  .dw LeadSynth_RD_15
  .dw silent_16

Beeps_0:
  .db 5,7,9,13,ARP_TYPE_ABSOLUTE
  .db 10,ENV_STOP
  .db 0,ENV_STOP
  .db 128,128,DUTY_ENV_LOOP,9
  .db ENV_STOP

S03_1:
  .db 5,7,9,11,ARP_TYPE_ABSOLUTE
  .db 10,ENV_STOP
  .db 0,ENV_STOP
  .db 0,DUTY_ENV_STOP
  .db ENV_STOP

Noise_2:
  .db 5,14,16,18,ARP_TYPE_ABSOLUTE
  .db 10,10,0,0,0,0,0,0,ENV_STOP
  .db 0,ENV_STOP
  .db 0,DUTY_ENV_STOP
  .db ENV_STOP

S03_3:
  .db 5,14,16,18,ARP_TYPE_ABSOLUTE
  .db 5,5,5,0,0,0,0,0,ENV_STOP
  .db 0,ENV_STOP
  .db 0,DUTY_ENV_STOP
  .db ENV_STOP

S08_4:
  .db 5,18,20,22,ARP_TYPE_ABSOLUTE
  .db 5,5,5,5,5,5,5,5,0,0,0,0,ENV_STOP
  .db 0,ENV_STOP
  .db 0,DUTY_ENV_STOP
  .db ENV_STOP

S05_5:
  .db 5,14,16,18,ARP_TYPE_ABSOLUTE
  .db 5,5,5,5,5,0,0,0,ENV_STOP
  .db 0,ENV_STOP
  .db 0,DUTY_ENV_STOP
  .db ENV_STOP

G03_6:
  .db 5,14,16,18,ARP_TYPE_ABSOLUTE
  .db 0,0,0,5,5,5,5,5,ENV_STOP
  .db 0,ENV_STOP
  .db 0,DUTY_ENV_STOP
  .db ENV_STOP

Arp_9_7:
  .db 5,7,9,13,ARP_TYPE_ABSOLUTE
  .db 4,ENV_STOP
  .db 0,ENV_STOP
  .db 128,128,DUTY_ENV_LOOP,9
  .db 0,0,9,9,ENV_LOOP,13

Arp_8_8:
  .db 5,7,9,13,ARP_TYPE_ABSOLUTE
  .db 4,ENV_STOP
  .db 0,ENV_STOP
  .db 128,128,DUTY_ENV_LOOP,9
  .db 0,0,8,8,ENV_LOOP,13

Lead_9:
  .db 5,7,9,13,ARP_TYPE_ABSOLUTE
  .db 4,ENV_STOP
  .db 0,ENV_STOP
  .db 128,128,DUTY_ENV_LOOP,9
  .db ENV_STOP

Lead_S05_10:
  .db 5,14,16,20,ARP_TYPE_ABSOLUTE
  .db 4,4,4,4,4,0,0,0,ENV_STOP
  .db 0,ENV_STOP
  .db 128,128,DUTY_ENV_LOOP,16
  .db ENV_STOP

LeadSynth_11:
  .db 5,7,21,23,ARP_TYPE_ABSOLUTE
  .db 6,ENV_STOP
  .db 1,1,1,0,0,0,-1,-1,-1,0,0,0,ENV_LOOP,7
  .db 128,DUTY_ENV_STOP
  .db ENV_STOP

LeadSynth_S08_12:
  .db 5,21,35,37,ARP_TYPE_ABSOLUTE
  .db 6,6,6,6,6,6,6,6,6,6,6,6,6,6,0,ENV_STOP
  .db 1,1,1,0,0,0,-1,-1,-1,0,0,0,ENV_LOOP,21
  .db 128,DUTY_ENV_STOP
  .db ENV_STOP

LeadSynth_DL__13:
  .db 5,7,21,23,ARP_TYPE_ABSOLUTE
  .db 6,ENV_STOP
  .db 1,1,1,0,0,0,-1,-1,-1,0,0,0,ENV_LOOP,7
  .db 128,DUTY_ENV_STOP
  .db 0,0,0,0,0,2,2,2,2,2,0,0,0,0,0,-2,-2,-2,-2,-2,-2,-2,-2,ENV_STOP

LeadSynth_RU_14:
  .db 5,7,21,23,ARP_TYPE_ABSOLUTE
  .db 6,ENV_STOP
  .db 1,1,1,0,0,0,-1,-1,-1,0,0,0,ENV_LOOP,7
  .db 128,DUTY_ENV_STOP
  .db 0,0,2,2,4,4,5,5,7,7,9,9,10,10,12,12,ENV_STOP

LeadSynth_RD_15:
  .db 5,23,37,39,ARP_TYPE_ABSOLUTE
  .db 6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,0,ENV_STOP
  .db 1,1,1,0,0,0,-1,-1,-1,0,0,0,ENV_LOOP,23
  .db 128,DUTY_ENV_STOP
  .db 0,0,-2,-2,-4,-4,-5,-5,-7,-7,-9,-9,-10,-10,-12,-12,ENV_STOP

silent_16:
  .db 5,7,9,11,ARP_TYPE_ABSOLUTE
  .db 0,ENV_STOP
  .db 0,ENV_STOP
  .db 0,DUTY_ENV_STOP
  .db ENV_STOP

_forest:
  .db 46
  .db 8
  .db 209
  .db 6
  .dw _forest_square1
  .dw _forest_square2
  .dw _forest_triangle
  .dw _forest_noise
  .dw 0

_forest_square1:
_forest_square1_loop:
  .db CAL,low(_forest_square1_0),high(_forest_square1_0)
  .db CAL,low(_forest_square1_0),high(_forest_square1_0)
  .db CAL,low(_forest_square1_1),high(_forest_square1_1)
  .db CAL,low(_forest_square1_1),high(_forest_square1_1)
  .db CAL,low(_forest_square1_0),high(_forest_square1_0)
  .db CAL,low(_forest_square1_0),high(_forest_square1_0)
  .db CAL,low(_forest_square1_1),high(_forest_square1_1)
  .db CAL,low(_forest_square1_1),high(_forest_square1_1)
  .db GOT
  .dw _forest_square1_loop

_forest_square2:
_forest_square2_loop:
  .db CAL,low(_forest_square2_0),high(_forest_square2_0)
  .db CAL,low(_forest_square2_0),high(_forest_square2_0)
  .db CAL,low(_forest_square2_5),high(_forest_square2_5)
  .db CAL,low(_forest_square2_5),high(_forest_square2_5)
  .db CAL,low(_forest_square2_1),high(_forest_square2_1)
  .db CAL,low(_forest_square2_2),high(_forest_square2_2)
  .db CAL,low(_forest_square2_3),high(_forest_square2_3)
  .db CAL,low(_forest_square2_4),high(_forest_square2_4)
  .db GOT
  .dw _forest_square2_loop

_forest_triangle:
_forest_triangle_loop:
  .db CAL,low(_forest_triangle_0),high(_forest_triangle_0)
  .db CAL,low(_forest_triangle_1),high(_forest_triangle_1)
  .db CAL,low(_forest_triangle_2),high(_forest_triangle_2)
  .db CAL,low(_forest_triangle_3),high(_forest_triangle_3)
  .db CAL,low(_forest_triangle_0),high(_forest_triangle_0)
  .db CAL,low(_forest_triangle_1),high(_forest_triangle_1)
  .db CAL,low(_forest_triangle_2),high(_forest_triangle_2)
  .db CAL,low(_forest_triangle_3),high(_forest_triangle_3)
  .db GOT
  .dw _forest_triangle_loop

_forest_noise:
_forest_noise_loop:
  .db CAL,low(_forest_noise_0),high(_forest_noise_0)
  .db CAL,low(_forest_noise_0),high(_forest_noise_0)
  .db CAL,low(_forest_noise_0),high(_forest_noise_0)
  .db CAL,low(_forest_noise_0),high(_forest_noise_0)
  .db CAL,low(_forest_noise_0),high(_forest_noise_0)
  .db CAL,low(_forest_noise_0),high(_forest_noise_0)
  .db CAL,low(_forest_noise_0),high(_forest_noise_0)
  .db CAL,low(_forest_noise_0),high(_forest_noise_0)
  .db GOT
  .dw _forest_noise_loop

_forest_square1_0:
  .db STI,7,SL0,D3,STI,8,E3,STI,7,F3,STI,8,E3
  .db RET

_forest_square1_1:
  .db STI,7,SL0,G3,STI,8,A3,B3,A3
  .db RET

_forest_square2_0:
  .db STI,9,SLF,G2,STI,10,SL1,G2,STI,9,SLF,G2,STI,10,SL1,G2,STI,9
  .db SLF,G2,STI,10,SL1,G2,STI,9,SLF,G2,STI,10,SL1,G2
  .db RET

_forest_square2_1:
  .db STI,11,SLE,B3,SL2,C4,SLC,B3,SL2,C4,B3,SLE,D4,SL2,E4,SL0
  .db D4
  .db RET

_forest_square2_2:
  .db STI,16,SL2,A0,STI,12,SL3,D4,STI,11,D4,C4,D4,SL2,C4,SLE,B3
  .db STI,13,SLL,32,A3,STI,14,SL2,G3
  .db RET

_forest_square2_3:
  .db STI,11,SLL,18,G4,SL3,G4,F4,SL6,E4,SL2,F4,SLE,E4,SL2,F4,SL0
  .db G4
  .db RET

_forest_square2_4:
  .db STI,16,SL2,A0,STI,12,SL3,G4,STI,11,G4,F4,G4,SL2,F4,SLE,E4
  .db STI,13,SLL,32,D4,STI,15,SL2,C4
  .db RET

_forest_square2_5:
  .db STI,9,SLF,C3,STI,10,SL1,C3,STI,9,SLF,C3,STI,10,SL1,C3,STI,9
  .db SLF,C3,STI,10,SL1,C3,STI,9,SLF,C3,STI,10,SL1,C3
  .db RET

_forest_triangle_0:
  .db STI,0,SL2,G1,SL1,G2,STI,3,G1,STI,0,SL2,G1,STI,3,SL1,D1,STI,5
  .db SL2,G1,STI,0,SL1,D1,SL4,G1,SL1,D2,E2,SL2,G1,STI,3,SL1,D1
  .db G1,STI,0,SL2,G1,STI,3,SL1,D1,STI,4,SL2,G1,STI,0,SL1,G1,STI,3
  .db D1,D1,B1,STI,0,B1,STI,3,D1,D1,STI,0,SL2,G1,SL1,G2,STI,3
  .db G1,STI,0,SL2,G1,STI,3,SL1,D1,STI,5,SL2,G1,STI,0,SL1,D1,SL4
  .db G1,SL1,D2,E2,SL2,G1,STI,3,SL1,D1,G1,STI,0,SL2,G1,STI,6
  .db SL1,D1,STI,4,SL2,G1,STI,3,SL1,G1,STI,4,SL2,E1,E1,F1
  .db RET

_forest_triangle_1:
  .db STI,0,SL2,G1,SL1,G2,STI,3,G1,STI,0,SL2,G1,STI,3,SL1,D1,STI,5
  .db SL2,G1,STI,0,SL1,D1,SL4,G1,SL1,D2,E2,SL2,G1,STI,3,SL1,D1
  .db G1,STI,0,SL2,G1,STI,6,SL1,D1,STI,4,SL2,G1,STI,0,SL1,G1,STI,2
  .db SL2,E1,STI,0,G1,E1,G1,SL1,G2,STI,3,G1,STI,0,SL2,G1,STI,3
  .db SL1,D1,STI,5,SL2,G1,STI,0,SL1,D1,SL4,G1,SL1,D2,E2,SL2,G1
  .db SL1,G2,D1,STI,3,G1,STI,0,E2,D1,STI,3,G1,STI,0,G1,D1,B1,STI,3
  .db G1,STI,0,G1,A1,SL2,B1
  .db RET

_forest_triangle_2:
  .db STI,0,SL2,C2,SL1,C3,STI,3,C2,STI,0,SL2,C2,STI,3,SL1,G1,STI,5
  .db SL2,C2,STI,0,SL1,G1,SL4,C2,SL1,G2,A2,SL2,C2,STI,3,SL1,G1
  .db C2,STI,0,SL2,C2,STI,3,SL1,G1,STI,4,SL2,C2,STI,0,SL1,C2,STI,3
  .db G1,G1,E2,STI,0,E2,STI,3,G1,G1,STI,0,SL2,C2,SL1,C3,STI,3
  .db C2,STI,0,SL2,C2,STI,3,SL1,G1,STI,5,SL2,C2,STI,0,SL1,G1,SL4
  .db C2,SL1,G2,A2,SL2,C2,STI,3,SL1,G1,C2,STI,0,SL2,C2,STI,6
  .db SL1,G1,STI,4,SL2,C2,STI,3,SL1,C2,STI,4,SL2,A1,A1,B1
  .db RET

_forest_triangle_3:
  .db STI,0,SL2,C2,SL1,C3,STI,3,C2,STI,0,SL2,C2,STI,3,SL1,G1,STI,5
  .db SL2,C2,STI,0,SL1,G1,SL4,C2,SL1,G2,A2,SL2,C2,STI,3,SL1,G1
  .db C2,STI,0,SL2,C2,STI,6,SL1,G1,STI,4,SL2,C2,STI,0,SL1,C2,STI,2
  .db SL2,A1,STI,0,C2,A1,C2,SL1,C3,STI,3,C2,STI,0,SL2,C2,STI,3
  .db SL1,G1,STI,5,SL2,C2,STI,0,SL1,G1,SL4,C2,SL1,G2,A2,C2,STI,5
  .db SL2,G2,STI,4,G2,F2,E2,E2,D2,D2,SL1,C2
  .db RET

_forest_noise_0:
  .db STI,2,SL2,1,SL1,1,1,SL2,12,SL1,1,SL2,1,SL1,1,SL2,1,12,1,1
  .db SL1,1,1,SL2,12,SL1,1,SL2,1,SL1,12,1,1,SL2,12,1,1,SL1,1,1,SL2
  .db 12,SL1,1,SL2,1,SL1,1,SL2,1,12,1,1,SL1,1,1,SL2,12,SL1,1,SL2
  .db 1,SL1,12,1,1,SL2,12,1
  .db RET

_sfx_fire:
  .db 0, 1
  .db 0, 1
  .dw 0
  .dw 0
  .dw 0
  .dw _sfx_fire_noise
  .dw 0

_sfx_fire_noise:
  .db CAL,low(_sfx_fire_noise_0),high(_sfx_fire_noise_0)
  .db TRM
_sfx_fire_noise_0:
  .db SLL,2,STI,0,1
  .db RET

