# Tiny Game #

Tiny Game is a very simple basic Game concept.
The simplest thing I could think of that would count as a videogame. 
Move, shoot, score, lose.

This is the latest iteration, being built for the NES!

### How do I get set up? ###

* [Grab NESASM3][1]
* Compile TinyGame.asm with it.
* Run TinyGame.nes in your favourite NES emulator

[1]: http://www.nespowerpak.com/nesasm/