	.INCLUDE "ggsound.inc"
	
	.inesprg 2   ; 1x 16KB PRG code
	.ineschr 1   ; 1x  8KB CHR data
	.inesmap 0   ; mapper 0 = NROM, no bank swapping
	.inesmir 0   ; background, horizontal mirroring

FIREBALLSPEED = $FF
	
; Zero Page declarations  
	.zp
	.rsset $0000
	.INCLUDE "ggsound_zp.inc"

; Controller states, Index ordered.
A.1.READ	.rs 1
B.1.READ	.rs 1
SEL.1.READ	.rs 1
STA.1.READ	.rs 1
UP.1.READ	.rs 1
DWN.1.READ	.rs 1
LFT.1.READ	.rs 1
RGT.1.READ	.rs 1

A.2.READ	.rs 1
B.2.READ	.rs 1
SEL.2.READ	.rs 1
STA.2.READ	.rs 1
UP.2.READ	.rs 1
DWN.2.READ	.rs 1
LFT.2.READ	.rs 1
RGT.2.READ	.rs 1

; Frame Data
PFrameFlg	.rs 1 ; 0 Walk1, 1 Walk2, 2 Attack, 4 Spell 
PFrameCnt	.rs 1
PFrmOfst	.rs 1

; Sprite Pointers
CSprite 	.rs 2 ; Pointer to the Sprite to be manipulated
CSprFrmFlg	.rs 2 ; Pointer to the Current Sprites Frame Flags
CSprFrmData	.rs 2 ; Pointer to the Current Sprites Frame Data
CFrmOfst	.rs 1 ; The Current Frame Offset
FrmYIndxTmp	.rs 2 ; Registers to store Y, when switching pointers
FrmXIndxTmp .rs 2 ; Registers to store X, when switching pointers
CFrameFlg	.rs 1 ; Temp storage for frame flags
FBPosTemp	.rs 2
rngSeed		.rs 2

	.bss
	.rsset $0700
; DMA declarations
Sage		.rs 16
FireBalls	.rs 64
Mobs		.rs 128

	.rsset $0200
	.INCLUDE "ggsound_ram.inc"
	
	.rsset $0500
PPOSY.LO	.rs 1
PPOSY.HI	.rs 1
PPOSX.LO	.rs 1
PPOSX.HI	.rs 1
PVELY.LO	.rs 1
PVELY.HI	.rs 1
PVELX.LO	.rs 1
PVELX.HI	.rs 1

MobCount	.rs 1
FBCount		.rs 1

FBYPostns	.rs 8 ; The Y positions of the Fireballs
FB1Vel		.rs 4
FB1FrmFlg	.rs 1
FBAtvFlag	.rs 1
FBInptDly	.rs 1
MobSpwnDly	.rs 1
FBIndex		.rs 1
MobYPstns	.rs 16
Mob1Vel		.rs 4
MobIndex	.rs 1
MobAtvFlag	.rs 1
RandomNum	.rs 4

	.CODE
	.BANK 0
	.ORG $8000
	.INCLUDE "Forest.asm"
	
	
	.BANK 1
	.ORG $A000
	.INCLUDE "ggsound.asm"
	.INCLUDE "prng.asm"
	
	.BANK 2
	.ORG $C000
palData:
	.db $19,$32,$13,$00,$19,$35,$36,$37,$19,$39,$3A,$3B,$19,$3D,$3E,$0F ;BG Palette
	.db $19,$07,$36,$30,$0f,$0f,$14,$28,$0f,$27,$16,$05,$0f,$2d,$28,$17 ;Sprite Palette
SageSpriteData:
	.db $D0,$00,$00,$80 ;Frame 1
	.db $D0,$01,$00,$88
	.db $D8,$10,$00,$80
	.db $D8,$11,$00,$88
	
	;Frame Data stored as Tile x4, Attributes x4
SageAnimationFrames:
	.db $00,$01,$10,$11,$00,$00,$00,$00 ;Walk1
	.db $20,$21,$30,$31,$00,$00,$00,$00 ;Walk2
	.db $02,$03,$12,$13,$00,$00,$00,$00 ;Attack
	.db $22,$23,$32,$33,$03,$03,$03,$03 ;Spell
	
FireballFrames:
	.db $04,$05,$14,$15,$02,$02,$02,$02
	.db $24,$25,$34,$35,$02,$02,$02,$02

BoarManFrames:
	.db $44,$45,$54,$55,$03,$03,$03,$03
	.db $55,$54,$45,$44,$43,$43,$43,$43
	
	
	.BANK 3
	.ORG $E000
	
MAIN:
	; Reset/Initialisation
	SEI
	CLD
	LDA #$40	;Disable APU frame IRQ.
    STA $4017
	LDX #$FF
	TXS			; Reset Stack
	INX 		; Reset X
	
	lda #$00	; Disable DMC IRQ
    sta $4010
	
	STX $2000
	STX $2001 	; Switch off PPU
	JSR vBlankWait
	JSR clrMem
	JSR vBlankWait
	JSR palette
	JSR spriteInit
	JSR ppuInit
	JSR initSound
	LDA #0
	STA sound_param_byte_0
	JSR play_song
END:
	JMP END
	
vBlankWait:
	BIT $2002
	BPL vBlankWait
	RTS
	
initSound:
; Here we initialide the GGSound Engine.
	LDA #0
	STA sound_param_byte_0
	LDA #low(song_list)
	STA sound_param_word_0
	LDA #high(song_list)
	STA sound_param_word_0+1
	LDA #low(sfx_list)
	STA sound_param_word_1
	LDA #high(sfx_list)
	STA sound_param_word_1+1
	LDA #low(instrument_list)
	STA sound_param_word_2
	LDA #high(instrument_list)
	STA sound_param_word_2+1
	JSR sound_initialize
	RTS
	
clrMem:
	LDA #$00
	STA $0000, x ; Clear the Zero Page
	STA $0200, x ; Clear the rest of User Memory
	STA $0300, x
	STA $0400, x
	STA $0500, x
	STA $0600, x
	LDA #$FE
	STA $0700, x ; Use this for DMA, clear to a blank tile
	INX
	BNE clrMem
	RTS

palette:
	LDA #$3f
	STA $2006
	LDA #0
	STA $2006
	TAX
doPalette:
	LDA palData,X
	STA $2007		;read and record the data
	INX		;increment read offset...
	CPX #$20
	BNE doPalette
	RTS

spriteInit:
	LDA #$80
	STA PPOSX.HI
	LDA #$D0
	STA PPOSY.HI
	LDA #0
	STA PPOSX.LO
	STA PPOSY.LO
	STA PVELX.HI
	STA PVELX.LO
	STA PVELY.HI
	STA PVELY.LO
	STA FBIndex
	LDX #$00
	
initSage:
	LDA SageSpriteData,x
	STA Sage,x
	INX
	CPX #$10
	BNE initSage
	RTS
	
ppuInit:
	JSR vBlankWait
	LDA #%10010000
	STA $2000
	LDA #%00011000
	STA $2001
	RTS	
	
NMI:
	; Push DMA to PPURAM
	LDA #$07
	STA $4014
	soundengine_update 
	; Program Code
	CLC
	LDA LOW(rngSeed)
	ADC #1
	LDA HIGH(rngSeed)
	ADC #0
	JSR resetVel
	JSR readJoypad
	JSR processInput
	JSR applyInput
	JSR loadSagePointer
	JSR moveSprite
	JSR updateAnimation
	LDA <A.1.READ
	BEQ .noFireball
	LDA FBInptDly
	CMP #45
	BCC .noFireball
	JSR spawnFireball
.noFireball
	LDA <STA.1.READ
	BEQ .noMob
	LDA MobSpwnDly
	CMP #45
	BCC .noMob
	JSR spawnBoarMan
.noMob
	JSR updateFireballs
	JSR updateMobs
.done
	CLC
	
INT:
	RTI ; Standard IRQ, NOP	

loadSagePointer:
	LDA #LOW(Sage)
	STA <CSprite
	LDA #HIGH(Sage)
	STA <CSprite+1
	LDA #LOW(SageAnimationFrames)
	STA <CSprFrmData
	LDA #HIGH(SageAnimationFrames)
	STA <CSprFrmData+1
	LDA <PFrameFlg
	STA <CFrameFlg
	
resetVel:
	LDA #0
	LDY #0
.loopReset
	STA PVELX.LO,y
	INY
	CPY #4
	BNE .loopReset
	RTS

readJoypad:
	LDX #$01
	STX $4016
	STX $4017
	DEX
	STX $4016
	STX $4017
	LDX #$00
doReadJoypad:
	LDA $4016
	AND #$01
	STA <A.1.READ,x
	LDA $4017
	AND #$01
	STA <A.2.READ,x
	INX
	CPX #$08
	BNE doReadJoypad
	RTS

processInput:
;	CLC
;	LDA <UP.1.READ
;	EOR <DWN.1.READ
;	BEQ downReleased
;	LDA <UP.1.READ
;	BEQ upReleased
;	SEC
;	LDA PVELY.LO
;	SBC #3
;	STA PVELY.LO
;	LDA PVELY.HI
;	SBC #0
;	STA PVELY.HI
;upReleased:
;	LDA <DWN.1.READ
;	BEQ downReleased
;	CLC
;	LDA PVELY.LO
;	ADC #3
;	STA PVELY.LO
;	LDA PVELY.HI
;	ADC #0
;	STA PVELY.HI
;downReleased:
	LDA <LFT.1.READ
	EOR <RGT.1.READ
	BEQ rightReleased
	LDA <LFT.1.READ
	BEQ leftReleased
	SEC
	LDA PVELX.LO
	SBC #$A0
	STA PVELX.LO
	LDA PVELX.HI
	SBC #0
	STA PVELX.HI
leftReleased:
	LDA <RGT.1.READ
	BEQ rightReleased
	CLC
	LDA PVELX.LO
	ADC #$A0
	STA PVELX.LO
	LDA PVELX.HI
	ADC #0
	STA PVELX.HI
rightReleased:
	LDA <A.1.READ
	BEQ .aReleased
	LDA #$03
	STA <PFrameFlg
	LDA #0
	STA <PFrameCnt
	JMP .loadFrame
	JMP .bReleased
.aReleased
	LDA <B.1.READ
	BEQ .bReleased
	LDA #$02
	STA <PFrameFlg
	LDA #0
	STA <PFrameCnt
.loadFrame
	JSR loadSagePointer
	JSR loadFrame
.bReleased
	LDA <STA.1.READ
	BEQ .startReleased
.startReleased	
	RTS

applyInput:
	CLC
	LDA PPOSY.LO
	ADC PVELY.LO
	STA PPOSY.LO
	LDA PPOSY.HI
	ADC PVELY.HI
	STA PPOSY.HI
	STA Sage
	CLC
	LDA PPOSX.LO
	ADC PVELX.LO
	STA PPOSX.LO
	LDA PPOSX.HI
	ADC PVELX.HI
	STA PPOSX.HI
	STA Sage+3
	RTS
	
moveSprite:
	LDY #$00
	LDA [CSprite],y	; Grab block1.Y
	LDY #$04
	STA [CSprite],y	; Set block2.Y
	CLC
	ADC #$08
	LDY #$08
	STA [CSprite],y	; Set block3.Y
	LDY #$0C
	STA [CSprite],y	; Set block4.Y
	
	LDY #$03
	LDA [CSprite],y	; Grab block1.X
	LDY #$0B
	STA [CSprite],y	; Set block3.X
	CLC
	ADC #$08
	LDY #$07
	STA [CSprite],y	; set Block2.X
	LDY #$0F
	STA [CSprite],y	; set Block4.X
	RTS

updateAnimation:
	INC <PFrameCnt
	LDX #$20
	CPX <PFrameCnt
	BNE .endUpdateAnimation
	LDX #$00
	STX <PFrameCnt
	LDA <PFrameFlg
	CMP #$0
	BEQ .setWalk1
	STX <PFrameFlg
	JMP .swapFrame
.setWalk1:
	LDX #$01
	STX <PFrameFlg
.swapFrame:
	LDA <PFrameFlg
	STA <CFrameFlg
	JSR loadFrame
.endUpdateAnimation:
	RTS
	
loadFrame:
	LDA <CFrameFlg
	CMP #0
	BNE .frame1
	LDX #0
	STX <PFrmOfst
	JMP .doFrameLoad
.frame1:
	CMP #%00000001
	BNE .frame2
	LDX #$08
	STX <PFrmOfst
	JMP .doFrameLoad
.frame2:
	CMP #%00000010
	BNE .frame3
	LDX #$10
	STX <PFrmOfst
	JMP .doFrameLoad
.frame3:
	LDX #$18
	STX <PFrmOfst
.doFrameLoad:
	LDY <PFrmOfst
	LDX #$00
.frameLoadLoop:
	LDA [CSprFrmData],y ; Grab the Tile ID
	STY <FrmYIndxTmp	; Only Y can be used for indirect/indexed
	STX <FrmXIndxTmp
	LDY <FrmXIndxTmp
	INY
	STA [CSprite],y		; Store it at Sprite+1
	LDY <FrmYIndxTmp	
	INY
	INY
	INY
	INY
	LDA [CSprFrmData],y
	LDY <FrmXIndxTmp
	INY
	INY
	STA [CSprite],y
	LDY <FrmYIndxTmp
	INY
	TXA
	CLC
	ADC #$04
	TAX
	CPX #$10
	BNE .frameLoadLoop
	RTS 

setFrameIndexs:
	CPX #0
	BEQ .done
	CLC
	ADC #$10
	STY <FrmYIndxTmp
	ASL <FrmYIndxTmp
	LDY <FrmYIndxTmp
	DEX
	JMP setFrameIndexs
.done
	RTS

spawnBoarMan:
	JSR prng
	STA RandomNum
	JSR prng
	STA RandomNum+1
	LDX MobIndex
	LDA #00
	LDY #1
	STY <FrmYIndxTmp
	JSR setFrameIndexs
.begin
	TAY
	LDA <FrmYIndxTmp
	ORA MobAtvFlag
	STA MobAtvFlag
	LDA RandomNum
	STA Mobs,y
	LDA RandomNum+1
	STA Mobs+3,y
	LDA Mobs,y
	CMP #$B0
	BCC .noShift
	LDA #$B0
	STA Mobs,y
.noShift
	STY <FrmYIndxTmp
	CLC
	LDA #LOW(Mobs)
	ADC <FrmYIndxTmp
	STA <CSprite
	LDA #HIGH(Mobs)
	ADC #0
	STA <CSprite+1
	LDA #LOW(BoarManFrames)
	STA <CSprFrmData
	LDA #HIGH(BoarManFrames)
	STA <CSprFrmData+1
	JSR moveSprite
	LDX #0
	STX FB1FrmFlg
	STX <CFrameFlg
	JSR loadFrame
	LDA #0
	LDX MobIndex
	INX
	CPX #04
	BCC .done
	LDX #00
.done
	STX MobIndex
	RTS

updateMobs:
	LDA MobSpwnDly
	CMP #45
	BCS .setup
	CLC
	ADC #1
	STA MobSpwnDly
.setup
	LDX #0
	STX <FrmXIndxTmp
	LDX #1
	TXA
	LDY #0
.begin
	BIT MobAtvFlag
	BEQ .next
	STX <FrmXIndxTmp+1	; Stash the flag index
	LDX <FrmXIndxTmp		; Grab the Position offset
	LDA Mobs,y
	STA MobYPstns+1,x
	LDA MobYPstns,x
	CLC
	ADC #FIREBALLSPEED
	STA FBYPostns,x
	LDA FBYPostns+1,x
	ADC #0
	STA MobYPstns+1,x
	STA Mobs,y
	STY <FrmYIndxTmp ; Store Y
	INX
	STX <FrmXIndxTmp
	LDX <FrmXIndxTmp+1
	; First grab the pointer to the fireball sprites block.
	LDA #LOW(Mobs) 
	STA <CSprite
	LDA #HIGH(Mobs)
	STA <CSprite+1
	; Next we add our Offset (y) to the address in the pointer.
	CLC
	LDA <CSprite
	ADC <FrmYIndxTmp
	STA <CSprite
	LDA <CSprite+1
	ADC 0
	STA <CSprite+1
	; Then we call Mov Sprite
	JSR moveSprite
	LDY <FrmYIndxTmp ; Restore Y
	LDA Mobs,y
	CMP #$02
	BCS  .next
	TXA 
	EOR MobAtvFlag
	STA MobAtvFlag
.next
	TXA	; Grab X
	ASL A; Move the bit one over
	TAX	; Put it back
	TYA	; Grab Y
	CLC
	ADC #$10	; Move it over 16
	TAY
	CMP #$40
	BEQ .done
	TXA ; Put X in A for the flag test
	JMP .begin
.done
	RTS
	
spawnFireball:
	LDX FBIndex
	LDA #0
	LDY #1
	STY <FrmYIndxTmp
	JSR setFrameIndexs
.begin
	TAY
	LDA <FrmYIndxTmp
	ORA FBAtvFlag
	STA FBAtvFlag
	LDA Sage
	STA FireBalls,y
	LDX #3
	LDA Sage+3
	STA FireBalls+3,y
	LDA FireBalls,y
	SEC
	SBC #10
	STA FireBalls,y
	STY <FrmYIndxTmp
	CLC
	LDA #LOW(FireBalls)
	ADC <FrmYIndxTmp
	STA <CSprite
	LDA #HIGH(FireBalls)
	ADC #0
	STA <CSprite+1
	LDA #LOW(FireballFrames)
	STA <CSprFrmData
	LDA #HIGH(FireballFrames)
	STA <CSprFrmData+1
	JSR moveSprite
	LDX #0
	STX FB1FrmFlg
	STX <CFrameFlg
	JSR loadFrame
	LDA #0
	STA FBInptDly
	LDX FBIndex
	INX
	CPX #04
	BCC .done
	LDX #00
.done
	STX FBIndex
	RTS
	
updateFireballs:
	LDA FBInptDly
	CMP #45
	BCS .setup
	CLC
	ADC #1
	STA FBInptDly
.setup
	LDX #0
	STX <FrmXIndxTmp
	LDX #1
	TXA
	LDY #0
.begin
	BIT FBAtvFlag
	BEQ .next
	STX <FrmXIndxTmp+1	; Stash the flag index
	LDX <FrmXIndxTmp		; Grab the Position offset
	LDA FireBalls,y
	STA FBYPostns+1,x
	LDA FBYPostns,x
	SEC
	SBC #FIREBALLSPEED
	STA FBYPostns,x
	LDA FBYPostns+1,x
	SBC #0
	STA FBYPostns+1,x
	STA FireBalls,y
	STY <FrmYIndxTmp ; Store Y
	INX
	STX <FrmXIndxTmp
	LDX <FrmXIndxTmp+1
	; First grab the pointer to the fireball sprites block.
	LDA #LOW(FireBalls) 
	STA <CSprite
	LDA #HIGH(FireBalls)
	STA <CSprite+1
	; Next we add our Offset (y) to the address in the pointer.
	CLC
	LDA <CSprite
	ADC <FrmYIndxTmp
	STA <CSprite
	LDA <CSprite+1
	ADC 0
	STA <CSprite+1
	; Then we call Mov Sprite
	JSR moveSprite
	LDY <FrmYIndxTmp ; Restore Y
	LDA FireBalls+8,y
	CMP #$FF
	BCC  .next
	TXA 
	EOR FBAtvFlag
	STA FBAtvFlag
.next
	TXA	; Grab X
	ASL A; Move the bit one over
	TAX	; Put it back
	TYA	; Grab Y
	CLC
	ADC #$10	; Move it over 16
	TAY
	CMP #$40
	BEQ .done
	TXA ; Put X in A for the flag test
	JMP .begin
.done
	RTS
	
	.ORG $FFFA
	.DW NMI,MAIN,INT
	
	.BANK 4
	.ORG $0000
	.INCBIN "TinyGame.chr"